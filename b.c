#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void getBin(int num, char *str);





main(int argc, char *argv[])
{
	char *inFileString, *outFileString;
	FILE *inFilePtr, *outFilePtr;

	if (argc != 3) {
		fprintf(stderr, "error: usage: %s <machine-code-file hex> <machine-code-file bin> \n", argv[0]);
		exit (EXIT_FAILURE);
	}

	inFileString = argv[1];
	outFileString = argv[2];

	inFilePtr = fopen(inFileString, "r");
	if (inFilePtr == NULL) {
		fprintf(stderr, "error in opening %s\n", inFileString);
		exit (EXIT_FAILURE);
	}
	outFilePtr = fopen(outFileString, "w");
	if (outFilePtr == NULL) {
		fprintf(stderr, "error in opening %s\n", outFileString);
		exit (EXIT_FAILURE);
	}
	
	char c;
	char conv[5];
	char tmp[2];
  do {
		c = fgetc (inFilePtr);
		tmp[0]=c;
		tmp[1]='\0';
		getBin((int)strtol(tmp, NULL, 16), conv); //converte da stringa scritta in decimale a intero.. poi getBin converte da intero a binario
		
    //printf("%c %s\n",c, conv);
		if (c == '\n') fputc(c, outFilePtr);
		else if (c != EOF && c !=' ') fputs(conv, outFilePtr);
	} while (c != EOF);
	fclose(outFilePtr);
	fclose(inFilePtr);
	
	
	return EXIT_SUCCESS;
}


void getBin(int num, char *str)
{
  *(str+4) = '\0';
  int mask = 0x10; // << 1;
  while(mask >>= 1)
    *str++ = !!(mask & num) + '0';
}
